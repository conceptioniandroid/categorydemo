package com.conceptioni.categorydemo;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;


public class MainAdapter extends RecyclerView.Adapter<MainAdapter.Holder> {
    private ArrayList<MainModel> matchesArrayList;
    Context mContext;

    public MainAdapter(ArrayList<MainModel> matchesArrayList) {
        this.matchesArrayList = matchesArrayList;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
         mContext = parent.getContext();
        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return new Holder(layoutInflater.inflate(R.layout.row_layout, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull Holder holder, final int position) {

        holder.tilte.setText(matchesArrayList.get(position).getId());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        holder.rec_sub.setLayoutManager(linearLayoutManager);
        holder.rec_sub.setItemAnimator(new DefaultItemAnimator());
        holder.rec_sub.addItemDecoration(new DividerItemDecoration(mContext, DividerItemDecoration.HORIZONTAL));
        SubAdapter ad = new SubAdapter(matchesArrayList.get(position).getSunmodelArrayList());
        holder.rec_sub.setAdapter(ad);

    }

    @Override
    public int getItemCount() {
        return matchesArrayList.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView tilte;
        RecyclerView rec_sub;


        Holder(@NonNull View itemView) {
            super(itemView);
            tilte = itemView.findViewById(R.id.tilte);
            rec_sub = itemView.findViewById(R.id.recsub);
        }
    }
}
