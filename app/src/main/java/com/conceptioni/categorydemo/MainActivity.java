package com.conceptioni.categorydemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<MainModel> mainModelArrayList = new ArrayList<>();
    RecyclerView rec_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setData();
        setRecycle();
    }

    private void setRecycle() {
        rec_list = findViewById(R.id.rec);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rec_list.setLayoutManager(linearLayoutManager);
        rec_list.setItemAnimator(new DefaultItemAnimator());
        rec_list.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        MainAdapter mAdapter1 = new MainAdapter(mainModelArrayList);
        rec_list.setAdapter(mAdapter1);
    }

    private void setData() {
        Sunmodel sunmodel = new Sunmodel();
        sunmodel.setId("1");
        sunmodel.setValue("test");

        for (int i = 0; i < 7; i++) {
            MainModel mainModel = new MainModel();
            mainModel.setId("Title " + i);
            ArrayList<Sunmodel> sunmodelArrayList = new ArrayList<>();
            for (int j = 0; j < 5; j++) {
                Sunmodel sunmodel1 = new Sunmodel();
                sunmodel.setId(String.valueOf(j));
                sunmodel.setValue("Subtitle " + j);
                sunmodelArrayList.add(sunmodel1);
            }
            mainModel.setSunmodelArrayList(sunmodelArrayList);
            mainModelArrayList.add(mainModel);
        }

    }
}
