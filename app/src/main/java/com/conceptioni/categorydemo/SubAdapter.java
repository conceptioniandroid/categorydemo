package com.conceptioni.categorydemo;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;


public class SubAdapter extends RecyclerView.Adapter<SubAdapter.Holder> {
    private ArrayList<Sunmodel> matchesArrayList;

    public SubAdapter(ArrayList<Sunmodel> matchesArrayList) {
        this.matchesArrayList = matchesArrayList;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context mContext = parent.getContext();
        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return new Holder(layoutInflater.inflate(R.layout.row_layout_sub, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull Holder holder, final int position) {

        holder.tilte.setText(matchesArrayList.get(position).getValue());

    }

    @Override
    public int getItemCount() {
        return matchesArrayList.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView tilte;

        Holder(@NonNull View itemView) {
            super(itemView);
            tilte = itemView.findViewById(R.id.tilte);
        }
    }
}
