package com.conceptioni.categorydemo;

import java.util.ArrayList;

public class MainModel {

    String id;
    ArrayList<Sunmodel> sunmodelArrayList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<Sunmodel> getSunmodelArrayList() {
        return sunmodelArrayList;
    }

    public void setSunmodelArrayList(ArrayList<Sunmodel> sunmodelArrayList) {
        this.sunmodelArrayList = sunmodelArrayList;
    }
}
